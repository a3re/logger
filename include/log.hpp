#pragma once

#include <tinyformat.h>

#define _LOG_LEVEL_OFF 0
#define _LOG_LEVEL_INFO 1
#define _LOG_LEVEL_WARN 2
#define _LOG_LEVEL_ERROR 3
#define _LOG_LEVEL_DEBUG 4

#if !defined(GLOBAL_LOG_LEVEL)
    #define GLOBAL_LOG_LEVEL _LOG_LEVEL_INFO
#endif

#if !defined(MODULE_NAME)
    #define MODULE_NAME "Unknown"
#endif

#define _LOG_VA_ARGS(...) , ##__VA_ARGS__
#define _LOG_GENERIC(level, f, ...) std::cout << tfm::format("[%20s:%-20s][" level "]" f "\n", MODULE_NAME, __func__ _LOG_VA_ARGS(__VA_ARGS__))

#if GLOBAL_LOG_LEVEL >= _LOG_LEVEL_INFO
    #define LOG_INFO(msg, ...) _LOG_GENERIC("INFO", msg, __VA_ARGS__)
#else
    #define LOG_INFO(msg, ...) (void)0
#endif

#if GLOBAL_LOG_LEVEL >= _LOG_LEVEL_WARN
    #define LOG_WARN(msg, ...) _LOG_GENERIC("WARN", msg, __VA_ARGS__)
#else
    #define LOG_WARN(msg, ...) (void)0
#endif

#if GLOBAL_LOG_LEVEL >= _LOG_LEVEL_ERROR
    #define LOG_ERROR(msg, ...) _LOG_GENERIC("ERROR", msg, __VA_ARGS__)
#else
    #define LOG_ERROR(msg, ...) (void)0
#endif

#if GLOBAL_LOG_LEVEL >= _LOG_LEVEL_DEBUG
    #define LOG_DEBUG(msg, ...) _LOG_GENERIC("DEBUG", msg, __VA_ARGS__)
#else
    #define LOG_DEBUG(msg, ...) (void)0
#endif